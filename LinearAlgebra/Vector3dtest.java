package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dtesting {
	Vector3d a = new Vector3d(5, 25, 10);
	Vector3d b = new Vector3d(10, 5, 25);


	@Test
	// Testing get methods
	public void getTest() {
		System.out.println(a.getX());
		System.out.println(a.getY());
		System.out.println(a.getZ());
	}
	
	@Test
	// Testing magnitude() method
	public void magnitudeTest() {
		System.out.println(a.magnitude(a));
	}
	
	@Test
	// Testing dotProduct() method
	public void dotProductTest() {
		System.out.println(a.dotProduct(a, b));
	}
	
	@Test
	// Testing add() method
	public void addTest() {
		System.out.println(a.add(a, b).getX());
		System.out.println(a.add(a, b).getY());
		System.out.println(a.add(a, b).getZ());
	}
	

}
