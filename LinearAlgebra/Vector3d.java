package LinearAlgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    } 

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude(Vector3d a) {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public double dotProduct(Vector3d a, Vector3d b) {
        return (a.getX() * b.getX()) + (a.getY() * b.getY()) + (a.getZ() * a.getZ());
    }

    public Vector3d add(Vector3d a, Vector3d b) {
        double vectorX = a.getX() + b.getX();
        double vectorY = a.getY() + b.getY();
        double vectorZ = a.getZ() + b.getZ();
        Vector3d c = new Vector3d(vectorX, vectorY, vectorZ);
        return c;
    }
}